﻿using System.Diagnostics;

namespace ConsoleApp1;

internal class Program
{
    public static async Task Main(string[] args)
    {
        int[] sizes = [100000, 1000000, 10000000];
        foreach (var size in sizes)
        {
            var array = new int[size];

            var rand = new Random();

            for (var i = 0; i < size; i++)
            {
                array[i] = rand.Next(1, 100);
            }

            var stopwatch = new Stopwatch();

            stopwatch.Start();
            var sum = Sum(array);
            stopwatch.Stop();
            Console.WriteLine($"Size: {size}, Method: Sequential, Sum: {sum}, Time: {stopwatch.ElapsedMilliseconds} ms");

            stopwatch.Restart();

            var parts = array.Length / 4; // Размер каждой части

            var part1 = new ArraySegment<int>(array, 0, parts).ToArray();
            var part2 = new ArraySegment<int>(array, parts, parts).ToArray();
            var part3 = new ArraySegment<int>(array, parts * 2, parts).ToArray();
            var part4 = new ArraySegment<int>(array, parts * 3, array.Length - parts * 3).ToArray();
            var parallelSum = await ParallelSum(part1, part2, part3, part4);
            stopwatch.Stop();
            Console.WriteLine($"Size: {size}, Method: Parallel, Sum: {parallelSum}, Time: {stopwatch.ElapsedMilliseconds} ms");

            stopwatch.Restart();
            var linqSum = LinqSum(array);
            stopwatch.Stop();
            Console.WriteLine($"Size: {size}, Method: LINQ, Sum: {linqSum}, Time: {stopwatch.ElapsedMilliseconds} ms");
        }
    }

    private static int Sum(IEnumerable<int> array)
    {
        return array.Sum();
    }

    private static async Task<int> ParallelSum(int[] array1, int[] array2, int[] array3, int[] array4)
    {
        var task1 = Task.Run(() => Sum(array1));
        var task2 = Task.Run(() => Sum(array2));
        var task3 = Task.Run(() => Sum(array3));
        var task4 = Task.Run(() => Sum(array4));

        await task1;
        await task2;
        await task3;
        await task4;

        return task1.Result + task2.Result + task3.Result + task4.Result;
    }

    private static int LinqSum(IEnumerable<int> array)
    {
        return array.AsParallel().Sum();
    }
}